class Node {
    constructor(name, x, y) {
        this.name = name;
        this.position = new THREE.Vector2(x, y);

        this.connectionsDrawn = [];
    }

}

class NodeMap {
    constructor(scene, nodes, nodeConnections) {
        this.scene = scene;

        this.nodes = {};

        for(let index in nodes) {
            if(nodes.hasOwnProperty(index)) {
                this.nodes[nodes[index].name] = nodes[index];
            }
        }

        this.nodeConnections = nodeConnections;
    }

    getNodeByName(name) {
        return this.nodes[name];
    }

    getNodes() {
        return this.nodes;
    }

    getNodeConnections() {
        return this.nodeConnections;
    }
}

class RoadRenderer {
    constructor(scene, nodeMap) {
        this.nodeMap = nodeMap;
        this.scene = scene;
    }

    render(roadMaterial, roadTurnMaterial, roadCrossroadTMaterial) {
        let nodes = this.nodeMap.getNodes();
        let nodeConnections = this.nodeMap.getNodeConnections();

        this.renderRoads(nodes, nodeConnections, roadMaterial);
        this.renderIntersections(nodes, nodeConnections, roadTurnMaterial, roadCrossroadTMaterial);
    }

    renderRoads(nodes, nodeConnections, roadMaterial) {
        var geometry, plane, distance;

        for(let index in nodes) {
            if(nodes.hasOwnProperty(index)) {
                let node = nodes[index];
                for(let nodeConnectionIndex in nodeConnections[node.name]) {
                    if(nodeConnections[node.name].hasOwnProperty(nodeConnectionIndex)) {
                        let connectedNode = this.nodeMap.getNodeByName(nodeConnections[node.name][nodeConnectionIndex]);

                        if(node.connectionsDrawn.indexOf(connectedNode.name) === -1) {
                            // mark connection as drawn on another node
                            connectedNode.connectionsDrawn.push(node.name);

                            //      console.log(`Rendering ${node.name} to ${connectedNode.name}`);

                            distance = node.position.distanceTo(connectedNode.position);

                            geometry = new THREE.PlaneGeometry(distance,0.03);
                            plane = new THREE.Mesh(geometry, roadMaterial);

                            plane.position.x = node.position.x;
                            plane.position.z = node.position.y; // node is 2 dimensional


                            plane.rotation.x = 1.5 * Math.PI;

                            if(node.position.x == connectedNode.position.x) {
                                plane.position.z += plane.geometry.parameters.width/2;
                                plane.rotation.z = Math.PI/2;
                            } else {
                                plane.position.x += plane.geometry.parameters.width/2;
                            }

                            this.scene.add(plane);
                        }
                    }
                }
            }
        }
    }

    renderIntersections(nodes, nodeConnections, roadTurnMaterial, roadCrossroadTMaterial) {
        let nullMaterial = new THREE.MeshLambertMaterial({color: 0xcccccc});
        let geometry = new THREE.PlaneGeometry(0.03, 0.03);
        var mesh;

        for(let index in nodes) {
            if(nodes.hasOwnProperty(index)) {
                let node = nodes[index];
                let thisNodeConnections = nodeConnections[node.name];
                // if node is a turn (2 connections)
                if(thisNodeConnections.length == 2) {
                    let con1 = nodes[thisNodeConnections[0]], con2 = nodes[thisNodeConnections[1]];
                    mesh = new THREE.Mesh(geometry, roadTurnMaterial);

                    if(RoadRenderer.isBottomToLeft(node.position, con1.position, con2.position)) {
                        mesh.rotation.z = Math.PI * 1.5;
                    } else if(RoadRenderer.isTopToRight(node.position, con1.position, con2.position)) {
                        mesh.rotation.z = Math.PI/2;
                    } else if(RoadRenderer.isTopToLeft(node.position, con1.position, con2.position)) {
                        mesh.rotation.z = Math.PI;
                    }
                } else if(thisNodeConnections.length == 3) {
                    let con1 = nodes[thisNodeConnections[0]],
                        con2 = nodes[thisNodeConnections[1]],
                        con3 = nodes[thisNodeConnections[2]];
                    mesh = new THREE.Mesh(geometry, roadCrossroadTMaterial);

                    if(RoadRenderer.isCrossroadUp(node.position, con1.position, con2.position, con3.position)) {
                        mesh.rotation.z = Math.PI;
                    }
                } else {
                    mesh = new THREE.Mesh(geometry, nullMaterial);
                }

                mesh.position.x = node.position.x;
                mesh.position.y = 1e-5; // move on top of roads
                mesh.position.z = node.position.y;

                mesh.rotation.x = Math.PI*1.5;

                this.scene.add(mesh);
            }
        }
    }

    static isOnRightSide(n, nc) {
        return (n.x < nc.x && n.y == nc.y);
    }

    static isOnBottomSide(n, nc) {
        return (n.x == nc.x && n.y < nc.y);
    }

    static isOnTopSide(n, nc) {
        return (n.x == nc.x && n.y > nc.y);
    }

    static isOnLeftSide(n, nc) {
        return (n.x > nc.x && n.y == nc.y);
    }

    /**
     * *--*
     * |
     * *
     *
     * @param n
     * @param nc1
     * @param nc2
     * @returns {*}
     */
    static isBottomToRight(n, nc1, nc2) {
        return RoadRenderer.isOnRightSide(n, nc1) && RoadRenderer.isOnBottomSide(n, nc2)
            || RoadRenderer.isOnRightSide(n, nc2) && RoadRenderer.isOnBottomSide(n, nc1);
    }

    static isCrossroadDown(n, nc1, nc2, nc3) {
        return RoadRenderer.isOnLeftSide(n, nc1) && RoadRenderer.isOnBottomSide(n, nc2) && RoadRenderer.isOnRightSide(n, nc3)
            || RoadRenderer.isOnLeftSide(n, nc1) && RoadRenderer.isOnBottomSide(n, nc3) && RoadRenderer.isOnRightSide(n, nc2)
            || RoadRenderer.isOnLeftSide(n, nc2) && RoadRenderer.isOnBottomSide(n, nc1) && RoadRenderer.isOnRightSide(n, nc3)
            || RoadRenderer.isOnLeftSide(n, nc2) && RoadRenderer.isOnBottomSide(n, nc3) && RoadRenderer.isOnRightSide(n, nc1)
            || RoadRenderer.isOnLeftSide(n, nc3) && RoadRenderer.isOnBottomSide(n, nc1) && RoadRenderer.isOnRightSide(n, nc2)
            || RoadRenderer.isOnLeftSide(n, nc3) && RoadRenderer.isOnBottomSide(n, nc2) && RoadRenderer.isOnRightSide(n, nc1);
    }

    static isCrossroadUp(n, nc1, nc2, nc3) {
         return RoadRenderer.isOnLeftSide(n, nc1) && RoadRenderer.isOnTopSide(n, nc2) && RoadRenderer.isOnRightSide(n, nc3)
         || RoadRenderer.isOnLeftSide(n, nc1) && RoadRenderer.isOnTopSide(n, nc3) && RoadRenderer.isOnRightSide(n, nc2)
         || RoadRenderer.isOnLeftSide(n, nc2) && RoadRenderer.isOnTopSide(n, nc1) && RoadRenderer.isOnRightSide(n, nc3)
         || RoadRenderer.isOnLeftSide(n, nc2) && RoadRenderer.isOnTopSide(n, nc3) && RoadRenderer.isOnRightSide(n, nc1)
         || RoadRenderer.isOnLeftSide(n, nc3) && RoadRenderer.isOnTopSide(n, nc1) && RoadRenderer.isOnRightSide(n, nc2)
         || RoadRenderer.isOnLeftSide(n, nc3) && RoadRenderer.isOnTopSide(n, nc2) && RoadRenderer.isOnRightSide(n, nc1);
    }

    /**
     * *--*
     *    |
     *    *
     *
     * @param n
     * @param nc1
     * @param nc2
     * @returns {*}
     */
    static isBottomToLeft(n, nc1, nc2) {
        return RoadRenderer.isOnLeftSide(n, nc1) && RoadRenderer.isOnBottomSide(n, nc2)
            || RoadRenderer.isOnLeftSide(n, nc2) && RoadRenderer.isOnBottomSide(n, nc1);
    }

    /**
     *    *
     *    |
     * *--*
     *
     * @param n
     * @param nc1
     * @param nc2
     * @returns {*}
     */
    static isTopToLeft(n, nc1, nc2) {
        return RoadRenderer.isOnLeftSide(n, nc1) && RoadRenderer.isOnTopSide(n, nc2)
            || RoadRenderer.isOnLeftSide(n, nc2) && RoadRenderer.isOnTopSide(n, nc1);
    }

    /**
     * *
     * |
     * *--*
     *
     * @param n
     * @param nc1
     * @param nc2
     * @returns {*}
     */
    static isTopToRight(n, nc1, nc2) {
        return RoadRenderer.isOnRightSide(n, nc1) && RoadRenderer.isOnTopSide(n, nc2)
            || RoadRenderer.isOnRightSide(n, nc2) && RoadRenderer.isOnTopSide(n, nc1);
    }

}

export default class {
    constructor(scene, textures) {
        this.roadMaterial = new THREE.MeshLambertMaterial({map: textures.road});
        this.roadTurnMaterial = new THREE.MeshLambertMaterial({map: textures.roadTurn});
        this.roadCrossroadTMaterial = new THREE.MeshLambertMaterial({map: textures.crossroadT});

        this.scene = scene;
        var nodeMap = this.getRoadNodes();

        this.roadRenderer = new RoadRenderer(scene, nodeMap);

        this.roadRenderer.render(this.roadMaterial, this.roadTurnMaterial, this.roadCrossroadTMaterial);
    }

    getRoadConnections() {
        return {
            topLeft: ["topMiddle", "bottomLeft"],
            bottomLeft: ["bottomMiddle", "topLeft"],
            topMiddle: ["topLeft", "topRight", "bottomMiddle"],
            bottomMiddle: ["bottomLeft", "bottomRight", "topMiddle"],
            topRight: ["topMiddle", "bottomRight"],
            bottomRight: ["bottomMiddle", "topRight"]
        }
    }

    getRoadNodes() {
        return new NodeMap(this.scene,
            [
                new Node("topLeft", 0.05, 0.035),
                new Node("bottomLeft", 0.05, 0.145),
                new Node("topMiddle", 0.16, 0.035),
                new Node("bottomMiddle", 0.16, 0.145),
                new Node("topRight", 0.24, 0.035),
                new Node("bottomRight", 0.24, 0.145)
            ], this.getRoadConnections()
        );
    }
}