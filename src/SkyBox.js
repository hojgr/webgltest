var skyBox = null;

function createSkyBox(skyBoxTexture) {
    var [xSize, ySize, zSize] = [2, 2, 2];

    var [xMultiplier, zMultiplier] = [50, 50];

    skyBoxTexture.wrapS = skyBoxTexture.wrapT = THREE.RepeatWrapping;
    skyBoxTexture.repeat.set( xSize * xMultiplier, zSize * zMultiplier );

    var material = new THREE.MeshBasicMaterial({map: skyBoxTexture});
    material.side= THREE.BackSide;

    var skyBox = new THREE.Mesh(new THREE.CubeGeometry(xSize, ySize, zSize), material);
    skyBox.position.y += ySize/2 - 0.005;

    return skyBox;
}

export default function(skyBoxTexture) {
    if(skyBox == null) {
        skyBox = createSkyBox(skyBoxTexture);
    }

    return skyBox;
}