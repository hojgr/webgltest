/**
 * @author James Baicoianu / http://www.baicoianu.com/
 */

export default class { //function ( object, domElement ) {

    constructor(object) {
        this.object = object;
        this.speed = 0.05;

        this.keyboardState = {
            Forward: 0,
            Backward: 0,
            Left: 0,
            Right: 0,
            Up: 0,
            Down: 0,
            RightArrow: 0,
            LeftArrow: 0,
            UpArrow: 0,
            DownArrow: 0
        };

        this.keyboardMap = {
            W: "Forward",
            S: "Backward",
            A: "Left",
            D: "Right",
            R: "Up",
            F: "Down",
            RightArrow: "RightArrow",
            LeftArrow: "LeftArrow",
            UpArrow: "UpArrow",
            DownArrow: "DownArrow"
        };

        this.KEYS = {
            W: 87,
            S: 83,
            A: 65,
            D: 68,
            R: 82,
            F: 70,
            RightArrow: 39,
            LeftArrow: 37,
            UpArrow: 38,
            DownArrow: 40
        };

        window.addEventListener("keydown", this.keyDown.bind(this));
        window.addEventListener("keyup", this.keyUp.bind(this));
    }


    keyDown(evt) {
        for (var k in this.keyboardMap){
            if (this.keyboardMap.hasOwnProperty(k)) {
                if(evt.keyCode === this.KEYS[k]) {
                    this.keyboardState[this.keyboardMap[k]] = 1;
                    break;
                }
            }
        }
    };

    keyUp(evt) {
        for (var k in this.keyboardMap){
            if (this.keyboardMap.hasOwnProperty(k)) {
                if(evt.keyCode === this.KEYS[k]) {
                    this.keyboardState[this.keyboardMap[k]] = 0;
                    break;
                }
            }
        }
    };

    update(delta) {
        if(this.keyboardState.Forward) {
            this.object.position.z -= this.speed * delta;
        } else if(this.keyboardState.Backward) {
            this.object.position.z += this.speed * delta;
        }

        if(this.keyboardState.Left) {
            this.object.position.x -= this.speed * delta;
        } else if(this.keyboardState.Right) {
            this.object.position.x += this.speed * delta;
        }

        if(this.keyboardState.Up) {
            this.object.position.y += this.speed * delta;
        } else if(this.keyboardState.Down) {
            this.object.position.y -= this.speed * delta;
        }

        if(this.keyboardState.UpArrow) {
            this.object.rotation.x += this.speed * delta*10;
        } else if(this.keyboardState.DownArrow) {
            this.object.rotation.x -= this.speed * delta*10;
        }

        if(this.keyboardState.LeftArrow) {
            this.object.rotation.y += this.speed * delta*10;
        } else if(this.keyboardState.RightArrow) {
            this.object.rotation.y -= this.speed * delta*10;
        }
    };
};
