var textureLoader = new THREE.TextureLoader();

export default function(texture_urls, callback) {
    recursiveLoad(texture_urls, {}, callback);
}

function recursiveLoad(texture_urls, textures, callback) {
    if(texture_urls.length == 0) {
        callback(textures);
    }

    textureLoader.load(texture_urls[0], function(texture) {
        textures[texture_urls[0]] = texture;
        recursiveLoad(texture_urls.slice(1), textures, callback);
    });
}