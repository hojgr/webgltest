export function generateBuildingTexture(color, width, height, anisotropy) {
    var canvas = document.createElement('canvas');

    var multiplier = 300;

    var final_width = width * multiplier;
    if(final_width % 2)
        final_width += 1;

    var final_height = height * multiplier;
    if(final_height % 2)
        final_height += 1;

    canvas.width = final_width;
    canvas.height = final_height;
    var context = canvas.getContext('2d');

    context.fillStyle = color;
    context.fillRect(0, 0, final_width, final_height);

    for (var y = 2; y < final_height; y += 2) {
        for (var x = 0; x < final_width; x += 2) {
            var value = Math.floor(Math.random() * height * multiplier);
            context.fillStyle = 'rgb(' + [value, value, value].join(',') + ')';
            context.fillRect(x, y, 2, 1);
        }
    }

    var canvas2 = document.createElement('canvas');
    canvas2.width = 512;
    canvas2.height = 1024;
    var context2 = canvas2.getContext('2d');

    context2.imageSmoothingEnabled = false;
    context2.mozImageSmoothingEnabled = false;

    context2.drawImage(canvas, 0, 0, canvas2.width, canvas2.height);

    var buildingTexture = new THREE.Texture( canvas2 );
    buildingTexture.anisotropy  = anisotropy;
    buildingTexture.needsUpdate = true;

    return buildingTexture;
}