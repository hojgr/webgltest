import {generateBuildingTexture} from "./TextureGenerator"

export default function bakeBuildings(anisotropy) {
    function createCustomHighBuildingGeometry() {
        var base = createBuildingGeometry(0.08, 0.06, 0.08);
        var tower = createBuildingGeometry(0.04, 0.2, 0.04);

        base.merge(tower);

        return base;
    }

    function createBuildingGeometry(width, height, depth) {
        var geometry = new THREE.BoxGeometry(width, height, depth);

        geometry.applyMatrix(new THREE.Matrix4().makeTranslation(0, geometry.parameters.height / 2, 0));

        var n = 6;
        geometry.faces.splice(n, 2);
        geometry.faceVertexUvs[0].splice(n, 2);


        geometry.faceVertexUvs[0][4][0].set(0, 0);
        geometry.faceVertexUvs[0][4][2].set(0, 0);

        geometry.faceVertexUvs[0][5][2].set(0, 0);

        return geometry;
    }


    var buildings = [
        // center right
        {x: 0.05, y: 0.04, z: 0.02, xPos: 0.20, yPos: 0, zPos: 0.06, color: "rgb(245,241,249)"},
        {x: 0.05, y: 0.05, z: 0.02, xPos: 0.20, yPos: 0, zPos: 0.08, color: "rgb(238,236,230)"},
        {x: 0.05, y: 0.06, z: 0.02, xPos: 0.20, yPos: 0, zPos: 0.1, color: "rgb(234,244,252)"},
        {x: 0.05, y: 0.03, z: 0.02, xPos: 0.20, yPos: 0, zPos: 0.12, color: "rgb(231,237,235)"},

        // center left
        {
            customGeometry: createCustomHighBuildingGeometry(),
            x: 0.08,
            y: 0.2,
            xPos: 0.105,
            yPos: 0,
            zPos: 0.09,
            color: "rgb(254,240,248)"
        },

        // bottom row
        {x: 0.06, y: 0.07, z: 0.04, xPos: 0.225, yPos: 0, zPos: 0.18, color: "rgb(253,240,241)"},
        {x: 0.04, y: 0.06, z: 0.04, xPos: 0.175, yPos: 0, zPos: 0.18, color: "rgb(249,251,252)"},
        {x: 0.04, y: 0.05, z: 0.04, xPos: 0.135, yPos: 0, zPos: 0.18, color: "rgb(244,234,250)"},
        {x: 0.04, y: 0.07, z: 0.04, xPos: 0.095, yPos: 0, zPos: 0.18, color: "rgb(254,252,246)"},
        {x: 0.05, y: 0.04, z: 0.04, xPos: 0.05, yPos: 0, zPos: 0.18, color: "rgb(249,233,251)"},

        // TOP ROW
        {x: 0.06, y: 0.04, z: 0.04, xPos: 0.225, yPos: 0, zPos: 0, color: "rgb(252,238,239)"},
        {x: 0.08, y: 0.05, z: 0.04, xPos: 0.155, yPos: 0, zPos: 0, color: "rgb(241,230,250)"},
        {x: 0.04, y: 0.07, z: 0.04, xPos: 0.095, yPos: 0, zPos: 0, color: "rgb(231,232,241)"},
        {x: 0.039, y: 0.09, z: 0.04, xPos: 0.0555, yPos: 0, zPos: 0, color: "rgb(244,243,231)"},

        // left bank
        {x: 0.05, y: 0.05, z: 0.22, xPos: 0.011, yPos: 0, zPos: 0.09, color: "rgb(232,252,235)"},


        // right administration buildings
        {x: 0.05, y: 0.05, z: 0.1, xPos: 0.28, yPos: 0, zPos: 0.15, color: "#333333"},
        {x: 0.05, y: 0.065, z: 0.12, xPos: 0.28, yPos: 0, zPos: 0.04, color: "rgb(248,254,254)"}

    ];

    var bakedBuildings = [];

    for (var i = 0; i < buildings.length; i++) {
        var b = buildings[i];
        var material = new THREE.MeshPhongMaterial({map: generateBuildingTexture(b.color, b.x, b.y, anisotropy)});

        var geometry;
        if (b.hasOwnProperty("customGeometry")) {
            geometry = b.customGeometry;
        } else {
            geometry = createBuildingGeometry(b.x, b.y, b.z);
        }
        var cube = new THREE.Mesh(geometry, material);
        cube.position.x = b.xPos;
        cube.position.y = b.yPos;
        cube.position.z = b.zPos;

        cube.tags = ["building"];
        bakedBuildings.push(cube);
    }

    return bakedBuildings;
}
