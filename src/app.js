import FlyControls from "./FlyControls";
import bakeBuildings from "./BuildingGenerator";
import instantiateCamera from "./Camera";
import Light from "./Light";
import SkyBox from "./SkyBox";
import MouseHandler from "./MouseHandler";
import RoadManager from "./RoadManager";
import loadTextures from "./TextureLoader";

var width = window.innerWidth-4;
var height = window.innerHeight-4;

function bakeTextureUrls(prefix, postfix, textures) {
    var urls = [];

    for(let t of textures) {
        urls.push(prefix + t + postfix);
    }

    return urls;
}

var myTextureLoader = loadTextures(bakeTextureUrls("images/", ".png", ["road", "road-turn", "skybox", "crossroad-t"]), function(textures) {
    var clock = new THREE.Clock();
    var scene = new THREE.Scene();

    var camera = instantiateCamera(width, height);

    var controls = new FlyControls(camera);
    var renderer = new THREE.WebGLRenderer();
    renderer.setSize(width, height);

    document.body.appendChild(renderer.domElement);

    var mouseHandler = new MouseHandler(document, scene, camera, width, height);

    for(let v of bakeBuildings(renderer.getMaxAnisotropy())) {
        scene.add(v);
    }

    scene.add(SkyBox(textures["images/skybox.png"]));
    scene.add(Light);

    var roadManager = new RoadManager(scene, {
        road: textures["images/road.png"],
        roadTurn: textures["images/road-turn.png"],
        crossroadT: textures["images/crossroad-t.png"]
    });

    function render() {
        requestAnimationFrame(render);

        controls.update(clock.getDelta());

        mouseHandler.update();

        renderer.render(scene,camera);
    }

    render();

});