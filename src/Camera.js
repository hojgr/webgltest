export default function (width, height) {
    let camera = new THREE.PerspectiveCamera(45, width/height, 0.001, 1000);
    camera.position.x += 0.15;
    camera.position.z = 0.092;
    camera.position.y = 0.4;
    camera.rotation.x -= Math.PI/2;

    return camera;
}