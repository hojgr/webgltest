
export default class {
    /**
     * Object over which record mousemove (document, canvas, ...)
     *
     * @param object
     * @param scene
     * @param camera
     * @param canvasWidth
     * @param canvasHeight
     */
    constructor(object, scene, camera, canvasWidth, canvasHeight) {
        this.clickHandlers = [];
        this.hoverHandlers = [];

        this.canvasWidth = canvasWidth;
        this.canvasHeight = canvasHeight;

        this.scene = scene;
        this.camera = camera;

        this.raycaster = new THREE.Raycaster();

        this._mouse = new THREE.Vector2(0,0);
        this._mouse_activated = false;

        this.object = object;

        this.INTERSECTED = null;

        object.addEventListener( 'mousemove', this.onMouseMove.bind(this), false );
        object.addEventListener( 'click', this.onClick.bind(this), false);

        this.clickHandlers.push(this.recordClickPosition.bind(this));
    }

    onClick(event) {
        for(let handler of this.clickHandlers) {
            handler(event);
        }
    }

    onMouseMove( event ) {
        event.preventDefault();

        if(!this._mouse_activated) {
            this._mouse_activated = true;
        }

        this._mouse.x = ( event.clientX / this.canvasWidth ) * 2 - 1;
        this._mouse.y = - ( event.clientY / this.canvasHeight ) * 2 + 1;
    }

    recordClickPosition() {
        var clickMesh;
        this.raycaster.setFromCamera( this._mouse, this.camera );
        var intersects = this.raycaster.intersectObjects( this.scene.children );

        if ( intersects.length > 0) {
            var point = intersects[0].point;

            clickMesh = new THREE.Mesh(new THREE.SphereGeometry(0.001, 32, 32), new THREE.MeshBasicMaterial({color: 0xff0000}));

            clickMesh.position.x = point.x;
            clickMesh.position.y = point.y;
            clickMesh.position.z = point.z;

            console.log(point);


            this.scene.add(clickMesh);

            setTimeout(()=>this.scene.remove(clickMesh), 1000);

        }
    }

    update() {
        this.raycaster.setFromCamera( this._mouse, this.camera );

        if(!this._mouse_activated) {
            return false;
        }

        var intersects = this.raycaster.intersectObjects( this.scene.children );

        if ( intersects.length > 0 && intersects[ 0 ].object.hasOwnProperty("tags") && intersects[ 0 ].object.tags.indexOf("building") !== -1) {
            if ( this.INTERSECTED != intersects[ 0 ].object ) {

                if ( this.INTERSECTED ) this.INTERSECTED.material.color.setHex( this.INTERSECTED.currentHex );

                document.body.style.cursor = "pointer";

                this.INTERSECTED = intersects[ 0 ].object;
                this.INTERSECTED.currentHex = this.INTERSECTED.material.color.getHex();
                this.INTERSECTED.material.color.setHex( 0xcccccc );

            }

        } else {
            if ( this.INTERSECTED ) this.INTERSECTED.material.color.setHex( this.INTERSECTED.currentHex );


            document.body.style.cursor = "default";
            this.INTERSECTED = null;

        }
    }
}
